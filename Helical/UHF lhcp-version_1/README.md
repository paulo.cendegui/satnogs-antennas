Documentation
====
Documentation and step by step building guide can be found in our [OHAI guide](https://ohai.libre.space/project/helical-antenna-v1/hardware/).
